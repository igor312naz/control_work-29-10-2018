<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $category1 = new Category();
        $category1
            ->setName('Траспорт');

        $manager->persist($category1);
        $this->addReference('category1', $category1);

        $manager->flush();

        $category2 = new Category();
        $category2
            ->setName('Политика');
        $manager->persist($category2);
        $this->addReference('category2', $category2);
        $manager->flush();
    }
}
