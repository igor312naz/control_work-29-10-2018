<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $category1 = $this->getReference('category1');
        $user1 = $this->getReference('user1');
        $tag1 = $this->getReference('tag1');
        $comment1 = $this->getReference('comment1');

        $category2 = $this->getReference('category2');
        $user2 = $this->getReference('user2');
        $tag2 = $this->getReference('tag2');
        $comment2 = $this->getReference('comment2');



        $News1 = new News();
        $News1
            ->setTitle('Новости траспорта')
            ->setShortDescription('Начали ремонтировать дороги')
            ->setFullDescription('В Кыргызстане ремонитруют дороги!')
            ->setCategory($category1)
            ->setUser($user1)
            ->setTag($tag1)
            ->setComment($comment1)
            ->setImage('img1.jpg')
            ->setQuality('10')
            ->setRelevant('0')
            ->setPleasure('3')
            ->setDatePublication(new \DateTime('2018-09-07 12:00:00'));

        $manager->persist($News1);
        $this->addReference('news1', $News1);

        $manager->flush();

        $News2 = new News();
        $News2
            ->setTitle('Новости политики')
            ->setShortDescription('Прошли реформы')
            ->setFullDescription('В Кыргызстане прошли реформы!')
            ->setCategory($category2)
            ->setUser($user2)
            ->setTag($tag2)
            ->setComment($comment2)
            ->setImage('img2.jpg')
            ->setQuality('1')
            ->setRelevant('0')
            ->setPleasure('1')
            ->setDatePublication(new \DateTime('2018-09-08 12:00:00'));
        $manager->persist($News2);
        $this->addReference('news2', $News2);
        $manager->flush();
    }

    public function getDependencies()

    {
        return array(

            UserFixtures::class,
            TagFixtures::class,
            CategoryFixtures::class

        );
    }
}
