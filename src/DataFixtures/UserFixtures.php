<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $comment1 = $this->getReference('comment1');
        $comment2 = $this->getReference('comment2');


        $user1 = new User();
        $user1
            ->setEmail('test@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setFullName('Иванов Иван Иванович');

        $manager->persist($user1);
        $this->addReference('user1', $user1);
        $manager->flush();

        $user2 = new User();
        $user2
            ->setEmail('test2@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER'])
            ->setFullName('Семенов Семен Семеныч');

        $manager->persist($user2);
        $this->addReference('user2', $user2);
        $manager->flush();

        $admin = new User();
        $admin
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ->setFullName('Админ ');

        $manager->persist($admin);
        $this->addReference('admin', $admin);
        $manager->flush();
    }

    public function getDependencies()

    {
        return array(

            CommentFixtures::class

        );
    }

}
