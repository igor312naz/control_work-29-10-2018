<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $tag1 = new Tag();
        $tag1
            ->setName('дорога');

        $manager->persist($tag1);
        $this->addReference('tag1', $tag1);

        $manager->flush();

        $tag2 = new Tag();
        $tag2
            ->setName('политика');
        $manager->persist($tag2);
        $this->addReference('tag2', $tag2);
        $manager->flush();
    }

}
