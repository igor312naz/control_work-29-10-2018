<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $comment1 = new Comment();
        $comment1
            ->setDescription('Хорошая новость!!! Ура!')
        ;

        $manager->persist($comment1);
        $this->addReference('comment1', $comment1);

        $manager->flush();

        $comment2 = new Comment();
        $comment2
            ->setDescription('Горите в аду! (');
        $manager->persist($comment1);
        $this->addReference('comment2', $comment2);
        $manager->persist($comment2);

        $manager->flush();
    }


}
