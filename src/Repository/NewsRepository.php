<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getNewsByCategory($id)
    {


        return $this
            ->getEntityManager()
            ->createQuery('select t from App\Entity\News t where t.category =' . $id)
            ->getResult();

    }

    public function getNewsByTag($id)
    {


        return $this
            ->getEntityManager()
            ->createQuery('select t from App\Entity\News t where t.tag =' . $id)
            ->getResult();

    }

}
