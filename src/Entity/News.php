<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;



/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @Vich\Uploadable
 */
class News
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="news_photo", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $date_publication;

    /**
     * @ORM\Column(type="string", length=1024)
     * @var string
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $fullDescription;



    /**
     * @var Tag
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Tag", inversedBy="news")
     */
    private $tag;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="news")
     */
    private $category;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="news")
     */
    private $user;

    /**
     * @var Comment
     *
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="news")
     */
    private $comment;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $quality;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $relevant;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $pleasure;


    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->pleasure = 0;
        $this->quality = 0;
        $this->relevant = 0;

    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): News
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param File $imageFile
     * @return News
     */
    public function setImageFile(File $imageFile): ?News
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param \DateTime $date_create
     * @return News
     */
    public function setDate(\DateTime $date_create): News
    {
        $this->date = $date_create;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $shortDescription
     * @return News
     */
    public function setShortDescription(string $shortDescription): News
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param string $fullDescription
     * @return News
     */
    public function setFullDescription(string $fullDescription): News
    {
        $this->fullDescription = $fullDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $date_publication
     * @return News
     */
    public function setDatePublication(\DateTime $date_publication): News
    {
        $this->date_publication = $date_publication;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatePublication()
    {
        return $this->date_publication;
    }

    /**
     * @param Tag $tag
     * @return News
     */
    public function setTag(Tag $tag): ?News
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param Category $category
     * @return News
     */
    public function setCategory(Category $category): ?News
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param User $user
     * @return News
     */
    public function setUser(User $user): ?News
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Comment $comment
     * @return News
     */
    public function setComment(Comment $comment): ?News
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return Comment
     */
    public function getComment(): Comment
    {
        return $this->comment;
    }

    /**
     * @param int $quality
     * @return News
     */
    public function setQuality(int $quality): ?News
    {
        $this->quality = $quality;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param int $relevant
     * @return News
     */
    public function setRelevant(int $relevant): ?News
    {
        $this->relevant = $relevant;
        return $this;
    }

    /**
     * @return int
     */
    public function getRelevant()
    {
        return $this->relevant;
    }

    /**
     * @param int $pleasure
     * @return News
     */
    public function setPleasure(int $pleasure): ?News
    {
        $this->pleasure = $pleasure;
        return $this;
    }

    /**
     * @return int
     */
    public function getPleasure()
    {
        return $this->pleasure;
    }


}

