<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2024)
     */
    private $description;

    /**
     * @var News[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="comment")
     */
    private $news;


    public function __construct()
    {
        $this->news = new ArrayCollection();


    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return News[]
     */
    public function getNews(): array
    {
        return $this->news;
    }



    /**
     * @param News $news
     * @return Comment
     */
    public function addNews(News $news)
    {
        $this->news->add($news);
        return $this;
    }


    /**
     * @param string $description
     * @return Comment
     */
    public function setDescription(string $description): Comment
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }



}

