<?php


namespace App\Controller;


use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(NewsRepository $newsRepository)
    {


        $news = $newsRepository->findAll();



        return $this->render('index.html.twig', [
            'news' => $news
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(
        Request $request,
        ObjectManager $manager)
    {

        $news = new News();
        $form = $this->createForm(NewsType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() and $form->isValid()) {
            $data = $form->getData();

            $news->setImageFile($data['image']);
            $news->setShortDescription($data['short_description']);
            $news->setFullDescription($data['full_description']);
            $news->setTitle($data['title']);

            $this->get('vich_uploader.upload_handler');
            $manager = $this->getDoctrine()->getManager();

            $manager->persist($news);
            $manager->flush();
            return $this->redirectToRoute('homepage');


        }

        return $this->render('create_news.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/favorite/{id}}",requirements={"id": "\d+"}, name="favorite")
     * @param int $id
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function favoriteAction(int $id,
                                   ObjectManager $manager)
    {

        /*$news = new News();

        dump($id);
        dump($news);



        if ($id == 1) {

            $tempId = $news->getRelevant();
            $id = $tempId + 1;
            $news->setRelevant($id);
        } elseif ($id == 2) {
            $tempId = $news->getQuality();
            $id = $tempId + 1;
            $news->setQuality($id);
        } elseif ($id == 3) {
            $tempId = $news->getPleasure();
            $id = $tempId + 1;
            $news->setPleasure($id);
        } elseif ($id == 4) {
            $tempId = $news->getRelevant();
            $id = $tempId - 1;
            $news->setRelevant($id);
        } elseif ($id == 5) {
            $tempId = $news->getQuality();
            $id = $tempId - 1;
            $news->setQuality($id);
        } elseif ($id == 6) {
            $tempId = $news->getPleasure();
            $id = $tempId - 1;
            $news->setPleasure($id);
        } else {
            return $this->redirectToRoute('homepage');

        $manager = $this->getDoctrine()->getManager();

        $manager->persist($news);
        $manager->flush();

        }*/

        return $this->redirectToRoute('homepage');


    }

}