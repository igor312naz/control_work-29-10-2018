<?php


namespace App\Controller;


use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class SearchController extends Controller
{

    /**
     * @Route("/category/{id}",requirements={"id": "\d+"}, name="category")
     * @param $id integer
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(int $id, NewsRepository $newsRepository)
    {
        $news = $newsRepository->getNewsByCategory($id);

        return $this->render('category.html.twig', array(
            'news' => $news
        ));

    }

    /**
     * @Route("/tag/{id}",requirements={"id": "\d+"}, name="tag")
     * @param $id integer
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tagAction(int $id, NewsRepository $newsRepository)
    {
        $news = $newsRepository->getNewsByTag($id);

        return $this->render('tag.html.twig', array(
            'news' => $news
        ));

    }


    /**
     * @Route("/description/{id}",requirements={"id": "\d+"}, name="description")
     * @param $id integer
     * @param NewsRepository $newsRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function descriptionAction(int $id, NewsRepository $newsRepository)
    {
        $news = $newsRepository->find($id);

        return $this->render('full_description.html.twig', array(
            'news' => $news
        ));

    }



}