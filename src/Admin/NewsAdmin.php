<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NewsAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title', null)
            ->add('date_publication', null);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title', null)
            ->add('image', null, array('label' => 'Изображение', 'template' => 'SonataAdmin/list_mapper_twigs/list_image.html.twig'))
            ->add('short_description', TextType::class)
            ->add('full_description', TextType::class)
            ->add('quality')
            ->add('relevant')
            ->add('pleasure')
            ->add('date_publication', 'datetime', array('label' => 'Дата публикации', 'format' => 'd.m.Y'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null)
            ->add('date_publication', DateType::class, array('widget' => 'single_text'))
            ->add('short_description', TextType::class)
            ->add('imageFile', FileType::class, array('required' => false))
            ->add('full_description', TextType::class);

    }
}